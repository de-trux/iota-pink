static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#dfdde7", "#3f2e2e" },
	[SchemeSel] = { "#dfdde7", "#D8898C" },
	[SchemeOut] = { "#dfdde7", "#FFF1DF" },
};
