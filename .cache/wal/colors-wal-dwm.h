static const char norm_fg[] = "#dfdde7";
static const char norm_bg[] = "#3f2e2e";
static const char norm_border[] = "#9c9aa1";

static const char sel_fg[] = "#dfdde7";
static const char sel_bg[] = "#E5A09D";
static const char sel_border[] = "#dfdde7";

static const char urg_fg[] = "#dfdde7";
static const char urg_bg[] = "#D8898C";
static const char urg_border[] = "#D8898C";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
