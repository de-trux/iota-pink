const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#3f2e2e", /* black   */
  [1] = "#D8898C", /* red     */
  [2] = "#E5A09D", /* green   */
  [3] = "#E9B2AF", /* yellow  */
  [4] = "#E3B9B5", /* blue    */
  [5] = "#FECBC7", /* magenta */
  [6] = "#FFF1DF", /* cyan    */
  [7] = "#dfdde7", /* white   */

  /* 8 bright colors */
  [8]  = "#9c9aa1",  /* black   */
  [9]  = "#D8898C",  /* red     */
  [10] = "#E5A09D", /* green   */
  [11] = "#E9B2AF", /* yellow  */
  [12] = "#E3B9B5", /* blue    */
  [13] = "#FECBC7", /* magenta */
  [14] = "#FFF1DF", /* cyan    */
  [15] = "#dfdde7", /* white   */

  /* special colors */
  [256] = "#3f2e2e", /* background */
  [257] = "#dfdde7", /* foreground */
  [258] = "#dfdde7",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
