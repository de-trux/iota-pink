# detrux's Iota Pink dotfiles

![image info](./preview/01.png)
![image info](./preview/02.png)

## Programs Used
* `bspwm`, Window Manager
* `picom`, [compositor](https://github.com/yshui/picom)
* `feh`, image viewer / background setter
* `polybar`, heavily modified [adi1090x](https://github.com/adi1090x/polybar-themes) bar (material)
* `zsh` + `starship`, shell and prompt
* `dunst`, notification daemon
* `rofi`, application launcher and menu
* `mpd`+`mpc`+`ncmpcpp`, music player and daemon
* `networkmanager-dmenu`, network menu
* `brillo`, [brightness control](https://www.youtube.com/watch?v=pGOaSS8nEQA)
* `kitty` , terminal
* `conky`, [system monitor](https://www.gnome-look.org/p/1868958)
* `pfetch`+`neofetch`, system info fetch
* `pulseaudio`+`pipewire` audio things
* `pywal`, `scrot`, `thunar`, `firefox`, `discord-canary`, `vim`, `gedit`, `qt5ct`, `lxappearance`, `xfce4-settings`, etc..

## Details

My current setup works best if you want 125% scaling factor on HiDPI display, just make sure to remove/comment these things if you want normal 100% scaling

btw my current display is 1920x1080 on 12,5" eDP1 monitor

```
~/.Xresources
-----------------
Xft.dpi: 120
```
and
```
~/.config/polybar/config.ini
-----------------
dpi = 120
```

after that, you probably need to manually reconfigure the font size for each programs by yourself
### Fonts
* Fantasque Sans Mono
* JetBrains Mono Nerd
* Iosevka Nerd
* Nerd Font Symbols
* Material Design Icons
* Manjari

### Themes
* **GTK2/3** [Matcha Dark](https://github.com/vinceliuice/Matcha-gtk-theme) (Aliz)
* **QT** Default Breeze

### Icons
* Papirus icon dark variant

### Wallpaper
* [Zero Two from Hit Anime Darling in FranXX](https://www.wallpaperflare.com/minimalism-zero-two-darling-in-the-franxx-pink-hair-anime-wallpaper-capeq)



